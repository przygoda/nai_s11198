﻿#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include "gammaramp.h"
#include <iostream>

using namespace cv;
using namespace std;



//nazwa okna
char* window_name = "s11198";
double Klatka = 0.0;
double pKlatka = 0.0;
double diffKlatka = 0.0;
double czyDzien = 0.0;
int pi = 0;
int i = 0;
int stan = 0;
int moc = 10000;
int jasnoscDnia = 0;
vector<string> morse;
Mat frame, pframe, testdnia;
string slowoMorsa;




void open(VideoCapture &camera)//obiekt video capture
{
	camera.open(0);
	if (!camera.isOpened())
	{
		cout << "Kamera nie dziala";
		exit(1);
	}

	camera.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	camera.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
}

void transformFrame(Mat& frame){
	Mat frame_gray;
	cvtColor(frame, frame_gray, CV_BGR2GRAY); //zmiana obrazu na czarno-bialy
	
	if (jasnoscDnia == 1){// Jezeli jest dzien zastosuj threshold //2,184 //0,110 - wartosci threshold jakie dla mnie dzialaly

		threshold(frame_gray, frame_gray, 110, 255, 0);
	}

	flip(frame_gray, frame, 1);// obrot klatki

}

void translateMorse(){
	//arraye z alfabetem morsa w 1 i 2 i lacinski
	string alfabet[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", ".", ",", " " };
	string alfabetMorsa[] = { "12", "2111", "2121", "211", "1", "1121", "221", "1111", "11", "1222", "212", "1211", "22", "21", "222", "1221", "2212", "121", "111", "2", "112", "1112", "122", "2112", "2122", "2211", "121212", "221122", "0" };
	//0- spacja
	//1 - kropka
	//2 - kreska
	morse.push_back(slowoMorsa); //upewniam sie, ze wszystko zostalo wrzucone w wektor
	
	cout << "Baza mowi: " << endl;
	for (int i = 0; i < morse.size(); i++) //loop przez wszystkie slowa zczytane i alfabet morsea/ Jesli wartosc w morse'ie zostanie rozpoznana to jest zastepowana przez lacinski
	{
		for (int j = 0; j < 29; j++)
		{
			if (morse[i] == alfabetMorsa[j])
			{

				cout << alfabet[j];
			}
		}
	}
	cout << " STOP" << endl;
	morse.clear(); //na wszelki wypadek wyczyszczenie wektora
	cout << "Koniec transmisji." << endl;
}

void readLight(Mat&frame){
	
	Klatka = norm(frame);//noramlizacja w celu otrzymania liczbowej wartosci okreslajacej klatke (ja szukam jasnosci)

	diffKlatka = Klatka - pKlatka; //roznica wartosci pomiedzy klatka a poprzednia klatka- pozwala wykryc impulsy

	//if ktory zapobiega zczytaniu pierwszego impulsu jako dlugo trawjacego - bez niego i jest stale inkrementowane a pi = 0 dopoki nie bedzie impulsu
	if (i == 0){ pKlatka = Klatka; i++; return; } 
	if (diffKlatka > moc && stan == 0){ //jezeli roznica wartosci norm pomiedzy klatkami jest wyzsza od pewnej wartosci <moc> to swiatlo zostalo wlaczone
		stan = 1; //informacja, ze swiatlo zostalo wlaczone
		
		// i - pi informuje przez jaki czas swiatlo bylo wylaczone; jezeli bylo wylaczone przez wiecej niz 7 to jest to przerwa pomiedzy "literami" morsa
		if (i - pi >= 7){ 
			morse.push_back(slowoMorsa);//wrzuca do wektora "litere" morsa(np .-.)
			slowoMorsa = "";
		}
		if (i - pi > 20){ morse.push_back("0"); }// jezeli swiatlo bylo wylaczone dluzej niz 20 to jest to spacja pomiedzy slowami alfabetu lacinskiego

		pi = i; //od teraz pi wynosi tyle ile i w poprzednim 'biegu' petli


	}
	if (diffKlatka < -moc && stan == 1){////jezeli roznica wartosci norm pomiedzy klatkami jest nizsza od ujemnej wartosci <moc> swiatlo zostalo wylaczone
		stan = 0; //informacja, ze teraz lampka zostala wylaczona
		//i-pi daje informacje o tym jak dlugo lampka byla wlaczone
		if (i - pi <= 3.5){ slowoMorsa += "1"; }//i-pi krotsze od 3.5 to .
		else if (i - pi >= 4.9){ slowoMorsa += "2"; }//i-pi dluzsze od 4.9 to -

		pi = i;
			}


	pKlatka = Klatka;//pklatka to poprzednia klatka wartosc norm)
	i++;
	
}


int main(int argc, char** argv)
{

	CGammaRamp GammaRamp; //funkcja umozliwiajaca kontrole jasnosci
	VideoCapture camera;

	namedWindow(window_name, CV_WINDOW_AUTOSIZE);
	open(camera);
	char x;
	do{
		system("CLS");
		cout << "1. Odszyfruj morse'a." << endl;
		cout << "2. Kontrola jasnosci ekranu w zaleznosci od jasnosci otoczenia." << endl;
		cout << "3.Wylacz." << endl;
		x = _getch();



		bool esc = false;
		bool test_klatki = 0;
		switch (x)
		{
		case'1':
			cout << "Po zakonczeniu transmisji przycisnij esc, aby ja odszyfrowac." << endl;
			camera >> testdnia;
			czyDzien = norm(testdnia); //sprawdzanie jasnosci dnia przed glowna petla
			
			if (czyDzien > 70000){ jasnoscDnia = 1; }
			else { jasnoscDnia = 0; }
			while (esc == false){//główna pętla
				//wczytaj klatke

				test_klatki = camera.read(frame);
				if (!test_klatki)
				{
					cout << "Nie udalo sie zczytac klatki" << std::endl;
					exit(1);
				}
				if (GetAsyncKeyState(VK_ESCAPE))//wyjscie esc
				{
					system("CLS");
					break;
				}
				
				camera >> frame;
				transformFrame(frame);
				readLight(frame);

				imshow("s11198", frame);//wyswietl klatke
				waitKey(30);
			}

			translateMorse();
			system("pause");
			break;
			;
		case '2':
			cout << "Przycisnij esc, aby zakonczyc." << endl;
			while (esc == false){
				camera >> frame;
				Klatka = norm(frame);
				//	cout << Klatka << endl;

				if (Klatka > 100000){
					GammaRamp.SetBrightness(NULL, 128);
				}
				else if (Klatka > 50000 && Klatka < 100000){
					GammaRamp.SetBrightness(NULL, 64);
				}
				else if (Klatka <= 50000){
					GammaRamp.SetBrightness(NULL, 32);
				}
				if (GetAsyncKeyState(VK_ESCAPE))//wyjscie esc
				{
					system("CLS");
					break;
				}
			}
		case '3':
			GammaRamp.SetBrightness(NULL, 128);
			return 0;

		}
	} while (true);



}

